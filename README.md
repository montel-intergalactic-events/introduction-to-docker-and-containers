# Introduction to Docker and Containers 

Materials from a hands-on workshop titled Introduction to Docker and Containers. The workshop was organised in collaboration with Mimmit Koodaa community. The workshop was given by Constanza Escobar Foster from Montel Intergalactic.

To open the slides, download this repo and open the file `workshop.html` in your browser from your file explorer.